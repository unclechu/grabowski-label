#!/bin/bash

fuck() {
	if [ "$1" -ne 0 ]; then
		zenity --error
		exit 1
	fi
}

font_file=./font.ttf
if [ -f ./font.otf ]; then
	font_file=./font.otf
fi
if [ ! -f "$font_file" ]; then
	zenity --error --text='Font file not found!'
	exit 1
fi

select_files() {
	
	local files=$(zenity \
		--title 'Source files' \
		--file-selection --multiple \
		--separator="☇" \
		--file-filter='*.png *.jpg *.jpeg')
	fuck $?
	[ "$files" == "" ] && exit 1
	
	local dest_dir=$(zenity \
		--title 'Destination folder' \
		--file-selection --directory)
	fuck $?
	[ "$dest_dir" == "" ] && exit 1
	
	IFS='☇' read -ra files_arr <<< "$files"
	for file in "${files_arr[@]}"; do
		"$0" "$file" "$dest_dir"
	done
	zenity --info --text 'It is done!'
	return 0
}

if [ $# -eq 0 ]; then
	select_files
	exit $?
elif [ $# -ne 2 ]; then
	zenity --error --text="Incorrect arguments count: $#"
	exit 1
fi

img_file=$1
img_file_name=$(basename "$img_file")
fuck $?
img_file_name_without_ext=${img_file_name%.*}
new_img_dest_dir=$2

img_data=$(identify "$img_file")
fuck $?

img_size=$(echo "$img_data" | grep -o ' [0-9]\+x[0-9]\+ ' | sed 's/ //g')
fuck $?

img_h=${img_size#*x}
img_w=${img_size%x${img_h}}

btm_rect_h=75
top_rect_h=35

rect_color='#000000'
text_color='#ffffff'

text_sides_size=30
text_center_size=40

new_img_w=$img_w
new_img_h=$[img_h + top_rect_h + btm_rect_h]

top_rect_pos_x1=0
top_rect_pos_y1=0
top_rect_pos_x2=$[new_img_w]
top_rect_pos_y2=$[top_rect_h]

btm_rect_pos_x1=0
btm_rect_pos_y1=$[img_h + top_rect_h]
btm_rect_pos_x2=$[new_img_w]
btm_rect_pos_y2=$[new_img_h]

text_category=${img_file_name_without_ext%|*}
text_title=${img_file_name_without_ext#*|}
text_signature='Mark Korneev'

convert \
	-extent "${new_img_w}x${new_img_h}" \
	-size "${new_img_w}x${new_img_h}" \
	-background '#ffffff00' \
	-roll "+0+${top_rect_h}" \
	\
	-fill "$rect_color" \
	-draw "rectangle ${top_rect_pos_x1},${top_rect_pos_y1} ${top_rect_pos_x2},${top_rect_pos_y2}" \
	-draw "rectangle ${btm_rect_pos_x1},${btm_rect_pos_y1} ${btm_rect_pos_x2},${btm_rect_pos_y2}" \
	\
	-fill "$text_color" \
	-font "$font_file" -pointsize "$text_sides_size" -gravity southwest -draw "text 20,15 '${text_category}'" \
	-font "$font_file" -pointsize "$text_sides_size" -gravity southeast -draw "text 20,15 '${text_signature}'" \
	-font "$font_file" -pointsize "$text_center_size" -gravity south -draw "text 0,10 '${text_title}'" \
	\
	-gravity north \
	"$img_file" "$new_img_dest_dir/$img_file_name"
